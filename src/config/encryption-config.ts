export const MyEncryptionTransformerConfig = {
  key: 'e41c966f21f9e15aa802463f8924e6a3fe3e9751f201304213b2f845d8841d61',
  algorithm: 'aes-256-cbc',
  ivLength: 16,
  iv: 'aa5ac19190424b1d88f9419ef949ae56'
};