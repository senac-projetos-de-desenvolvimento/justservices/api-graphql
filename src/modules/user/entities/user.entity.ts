import { FilterableField } from '@nestjs-query/query-graphql';
import { ObjectType, Field, ID, GraphQLISODateTime, HideField } from '@nestjs/graphql';
import { AfterLoad, BeforeInsert, BeforeUpdate, Column, CreateDateColumn, DeleteDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

import * as bcrypt from 'bcrypt';
import { EncryptionTransformer } from 'typeorm-encrypted';
import { MyEncryptionTransformerConfig } from 'src/config/encryption-config';
@Entity()
@ObjectType()
export class User {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    transformer: new EncryptionTransformer(MyEncryptionTransformerConfig)
  })
  @FilterableField()
  name: string;

  @Column({
    unique: true,
    transformer: new EncryptionTransformer(MyEncryptionTransformerConfig)
  })
  @FilterableField()
  cpf: string;

  @Column({
    unique: true,
    transformer: new EncryptionTransformer(MyEncryptionTransformerConfig) 
  })
  @FilterableField()
  email: string;

  @Column({ nullable: true })
  @FilterableField()
  birthday: Date;

  @Column({ nullable: true })
  @FilterableField()
  genre: string;

  @Column({ 
    nullable: true, 
    transformer: new EncryptionTransformer(MyEncryptionTransformerConfig) 
  })
  @Field()
  cep: string;

  @Column({
    nullable: true,
    transformer: new EncryptionTransformer(MyEncryptionTransformerConfig) 
  })
  @Field()
  phone: string;

  @Column()
  @HideField()
  password: string;

  private tempPassword: string;

  @AfterLoad()
  private loadTempPassword(): void {
    this.tempPassword = this.password;
  }

  @BeforeUpdate()
  private encryptPassword(): void {
    if (this.tempPassword !== this.password) {
      this.password = bcrypt.hashSync(this.password, 10);
    }
  }

  @BeforeInsert()
  private encryptPasswordBeforeCreate(): void {
    this.password = bcrypt.hashSync(this.password, 10);
  }

  @CreateDateColumn()
  @Field(() => GraphQLISODateTime)
  createdAt: Date;

  @UpdateDateColumn()
  @Field(() => GraphQLISODateTime)
  updatedAt: Date;

  @DeleteDateColumn()
  @Field(() => GraphQLISODateTime, { nullable: true })
  deletedAt?: Date;
}
