import { CreateUserInput } from './create-user.input';
import { InputType, Field, PartialType } from '@nestjs/graphql';
import { IsEmail, IsOptional, IsString } from 'class-validator';

@InputType()
export class UpdateUserInput extends PartialType(CreateUserInput) {
  @Field()
  @IsString()
  @IsOptional()
  name?: string;

  @Field()
  @IsString()
  @IsOptional()
  cpf?: string;

  @Field()
  @IsEmail()
  @IsOptional()
  email?: string;

  @Field()
  @IsString()
  @IsOptional()
  passowrd?: string;

  @Field()
  @IsString()
  @IsOptional()
  birthday?: string;

  @Field()
  @IsString()
  @IsOptional()
  phone?: string;

  @Field()
  @IsString()
  @IsOptional()
  cep?: string;

  @Field()
  @IsString()
  @IsOptional()
  genre?: string;
}
